#!/usr/bin/env python
# -*- coding: utf-8 -*-

from six.moves import urllib
import datetime
from subprocess import call
from mastodon import Mastodon
import time
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator       # serveix per poder assignar el valor d'elements de un diccionari a una variable 15/07/18
import redis
import psycopg2
import math

from decimal import *
getcontext().prec = 2

def size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config/config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social

# Load configuration from config file
db_config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", db_config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", db_config_filepath)
mastats_db = get_parameter("mastats_db", db_config_filepath)
mastats_db_user = get_parameter("mastats_db_user", db_config_filepath)

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################
# Connectar amb la bbdd Postgres per obtenir el usuaris actius darrers 30 dies
# 12.9.19 quant espai ocupa la base de dades Mastodon
###############################################################################

try:

    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("select current_date, count(*) from users where last_sign_in_at > current_date-30")
    rows = cur.fetchall()
    for row in rows:
        print("Número d'usuaris actius els darrers 30 dies :", row[1])
        actius30 = row[1]

    ############################################################################################
    # 12.9.19 *New* Get how many disc space is using the Mastodon's DB.
    cur.execute("select pg_database_size(%s)", (mastodon_db,))
    db_disk_space = cur.fetchone()[0]

    conn.close()

except:

    print("No em puc connectar a la base de dades " + mastodon_db + " :-(")

###############################################################################
# GET THE DATA
###############################################################################

###############################################################################
# usuaris actuals
###############################################################################

try:

    conn = None
    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("SELECT count(*) FROM users WHERE disabled='f' and approved='t'")

    row = cur.fetchone()
    if row != None:
        current_id = row[0]
    else:
        current_id = 0

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print (error)

finally:

  if conn is not None:

      conn.close()

###############################################################################

res = requests.get('https://' + mastodon_hostname + '/api/v1/instance?')
num_instances = res.json()['stats']['domain_count']
num_toots = res.json()['stats']['status_count']

# Toots per usuari
toots_per_usuari = int(num_toots / current_id)

print("Número d'usuaris: %s "% current_id)
print("Tuts publicats: %s "% num_toots)
print("Instàncies connectades: %s "% num_instances)
print("Tuts per usuari: %s "% toots_per_usuari)

###############################################################################

ara = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

###############################################################################
# Redis 25/10/18 connectar amb Redis per a obtenir l'activitat setmanal
###############################################################################

redis_host = "localhost"
redis_port = 6379
redis_password = ""

setmana_actual = str(datetime.datetime.now().isocalendar()[1])

r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

# Interaccions setmana actual

interaccions = r.get("activity:interactions:"+setmana_actual)
if interaccions == None:
   interaccions = 0
print("Interaccions d'aquesta setmana: %s "% interaccions)

actius = r.pfcount("activity:logins:"+setmana_actual)
print("Usuaris actius aquesta setmana: %s "% actius)

###############################################################################
# Connectar amb la bbdd Postgres per obtenir la darrera fila i els seus usuaris
###############################################################################

usuaris_abans = current_id
toots_abans = num_toots
instancies_abans = num_instances
usuarishora = 0
tootshora = 0
instancieshora = 0
toots_inici_setmana = num_toots
inc_disc_space_hour = 0
toots_actius = 0.00

try:

  conn = None
  conn = psycopg2.connect(database = mastats_db, user = mastats_db_user, password = "", host = "/var/run/postgresql", port = "5432")

  cur = conn.cursor()

  cur.execute("SELECT DISTINCT ON (datetime) users,toots,instances,datetime,used_disc_space FROM " + mastats_db + " WHERE datetime > current_timestamp - INTERVAL '70 minutes' ORDER BY datetime asc LIMIT 1")

  row = cur.fetchone()
  if row != None:
      usuaris_abans = row[0]
      toots_abans = row[1]
      instancies_abans = row[2]
      disc_space_before = row[4]
  else:
      usuaris_abans = current_id
      toots_abans = num_toots
      instancies_abans = num_instances
      disc_space_before = db_disk_space

  # quants toots en l'inici de la setmana
  cur.execute("SELECT DISTINCT ON (datetime) toots, datetime FROM " + mastats_db + "  WHERE datetime > date_trunc('week', now()::timestamp) ORDER by datetime asc LIMIT 1")
  row = cur.fetchone()
  if row == None:
    toots_inici_setmana = num_toots
  else:
    toots_inici_setmana = row[0]

  cur.close()

  usuarishora = current_id - usuaris_abans
  tootshora = num_toots - toots_abans
  instancieshora = num_instances - instancies_abans
  inc_disc_space_hour = db_disk_space - disc_space_before

except (Exception, psycopg2.DatabaseError) as error:

  print (error)

finally:

  if conn is not None:

      conn.close()

if toots_inici_setmana == num_toots:
  toots_actius = 0.00
elif actius == 0:
  toots_actius = 0.00
else:
  toots_actius = round((num_toots-toots_inici_setmana)/actius, 2)

print("-----------------")
print("current_id: "+str(current_id))
print("usuaris abans: "+str(usuaris_abans))
print("usuaris hora: "+str(usuarishora))
print("-----------------")
print("toots: "+str(num_toots))
print("toots abans: "+str(toots_abans))
print("toots x hora: "+str(tootshora))
print("-----------------")
print("instancies: "+str(num_instances))
print("instancies abans: "+str(instancies_abans))
print("instancies x hora: "+str(instancieshora))
print("-----------------")
print("toots aquesta setmana:"+str(num_toots-toots_inici_setmana))
print("usuaris actius:"+str(actius))
print("toots x actius: "+str(toots_actius))
print("-------------------------------------")
print("   spla@mastodont.cat @ 2018 - 2020   ")
print("-------------------------------------")

######################################################################################################################################
# Connectar amb la bbdd de Postgres mastats_db per guardar les variables per a Grafana

inserta_linia = "INSERT INTO " + mastats_db + "(datetime, users, usershour, toots, tootshour, tootsuser, interactions, actives, actives30, instances, instanceshour, "
inserta_linia += "tootsactives, used_disc_space, disc_space_hour) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

conn = None

ara = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

try:

    conn = psycopg2.connect(database = mastats_db, user = mastats_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(inserta_linia, (ara, current_id, usuarishora, num_toots, tootshora, toots_per_usuari, interaccions, actius, actius30, num_instances, instancieshora, toots_actius, db_disk_space, inc_disc_space_hour))

    conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

  if conn is not None:

      conn.close()

###############################################################################
# WORK OUT THE TOOT TEXT
###############################################################################

# Calculate difference in times
hourly_change_string = ""
daily_change_string  = ""
weekly_change_string = ""

# canvi horari usuaris 19/05/18
users_hourly_change_string = ""

#######################################################################################################################
# Connectar amb la bbdd Postgres per obtenir increment usuaris en la darrera hora, el darrer dia i en la darrera setmana
########################################################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = mastats_db, user = mastats_db_user, password = "", host = "/var/run/postgresql", port = "5432")

  cur = conn.cursor()

  cur.execute("SELECT DISTINCT ON (datetime) users, datetime FROM " + mastats_db + " WHERE datetime > current_timestamp - INTERVAL '62 minutes' ORDER BY datetime asc LIMIT 1")

  row = cur.fetchone()
  if row[0] == None:
    usuaris_fa_una_hora = 0
  else:
    usuaris_fa_una_hora = row[0]

  cur.execute("SELECT DISTINCT ON (datetime) users, datetime FROM " + mastats_db + " WHERE datetime > current_timestamp - INTERVAL '1 day' ORDER BY datetime asc LIMIT 1")

  row = cur.fetchone()
  if row[0] == None:
    usuaris_fa_un_dia = 0
  else:
    usuaris_fa_un_dia = row[0]

  cur.execute("SELECT DISTINCT ON (datetime) users, datetime FROM " + mastats_db + " WHERE datetime > current_timestamp - INTERVAL '7 days' ORDER BY datetime asc LIMIT 1")

  row = cur.fetchone()
  if row[0] == None:
    usuaris_fa_una_setmana = 0
  else:
    usuaris_fa_una_setmana = row[0]

  cur.close()

except (Exception, psycopg2.DatabaseError) as error:
  print (error)
finally:
  if conn is not None:
      conn.close()

#########################################################################################################

inc_hora = current_id - usuaris_fa_una_hora
inc_dia = current_id - usuaris_fa_un_dia
inc_setmana = current_id - usuaris_fa_una_setmana

print("------------------------")
print("increment usuaris hora: "+str(inc_hora))
print("increment usuaris dia: "+str(inc_dia))
print("increment usuaris setmana: "+str(inc_setmana))
print("------------------------")
print("   spla @ 2018 - 2020   ")
print("------------------------")

###################################################################################

# Hourly change
if inc_hora != 0:

  users_hourly_change = current_id - usuaris_fa_una_hora
  print("Evolució horaria usuaris: %s"%users_hourly_change)
  if users_hourly_change > 0:
    hourly_change_string = "+" + format(users_hourly_change, ",d") + " en la darrera hora\n"

    # Daily change
    if inc_dia != 0:

      daily_change = current_id - usuaris_fa_un_dia
      print("Evolució diaria: %s"%daily_change)
      if daily_change > 0:
        daily_change_string = "+" + format(daily_change, ",d") + " en el darrer dia\n"

    # Weekly change
    if inc_setmana != 0:

      weekly_change = current_id - usuaris_fa_una_setmana
      print("Evolució setmanal: %s"%weekly_change)
      if weekly_change > 0:
        weekly_change_string = "+" + format(weekly_change, ",d") + " en la darrera setmana\n"

  elif users_hourly_change < 0:

    hourly_change_string = format(users_hourly_change, ",d") + " en la darrera hora\n"

    # Daily change
    if inc_dia != 0:
      daily_change = current_id - usuaris_fa_un_dia
      print("Evolució diaria: %s"%daily_change)
      if daily_change < 0:
        daily_change_string = format(daily_change, ",d") + " en el darrer dia\n"

    # Weekly change
    if inc_setmana != 0:

      weekly_change = current_id - usuaris_fa_una_setmana
      print("Evolució setmanal: %s"%weekly_change)
      if weekly_change < 0:
        weekly_change_string = format(weekly_change, ",d") + " en la darrera setmana\n"

###############################################################################
# TOOT IT!
###############################################################################

toot_text = "Som " + str(current_id) + " usuaris\n"
toot_text += hourly_change_string + "\n"
toot_text += daily_change_string
toot_text += weekly_change_string
toot_text += "\n"
toot_text += "Toots publicats: %s "% num_toots + "\n"
toot_text += "Toots / hora: %s "% tootshora + "\n"
toot_text += "Toots / usuari: %s "% toots_per_usuari + "\n"
toot_text += "\n"
toot_text += "Activitat setmana actual" + "\n"
toot_text += "Interaccions: %s "% interaccions + "\n"
toot_text += "Usuaris actius: %s "% actius + "\n"
toot_text += "\n"
toot_text += "Instàncies connectades: %s "% num_instances + "\n"
toot_text += "Instàncies / hora: %s "% instancieshora + "\n"
toot_text += "\n"
toot_text +=  "Espai en disc de la BD: %s "% str(size(db_disk_space)) + "\n"
toot_text +=  "Espai en disc / hora: %s "% str(size(inc_disc_space_hour)) + "\n"

print("Tootejant...")
print(toot_text)
if len(toot_text) < 500:
    mastodon.status_post(toot_text, in_reply_to_id=None, )
else:
    toot_text1, toot_text2 = toot_text[:int(len(toot_text)/2)], toot_text[int(len(toot_text)/2):]
    toot_id = mastodon.status_post(toot_text1, in_reply_to_id=None,)
    mastodon.status_post(toot_text2, in_reply_to_id=toot_id,)
    print(toot_text1)
    print(toot_text2)

print("Tootejat amb èxit!")
