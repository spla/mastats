Graphical Mastodon's stats with Python + Postgresql + Grafana
============================================================

Python script that gets *realtime* stats data from [Mastodon](https://joinmastodon.org)'s DB.

### Dependencies

-   **Python 3**
-   Grafana server
-   Postgresql server 
-   Everything else at the top of `mastats.py`!

### Usage:

Within your Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install required Python libraries.

2. Run `python setup.py` to get your bot's access token of a Mastodon existing account. It will be saved to 'secrets/secrets.txt' for further use.

3. Run `python db-setup.py` to create mastats database and table.

4. Run `python mastats.py` to start collecting stats.

5. Use your favourite scheduling method to set `mastats.py` to run regularly.

6. Add the datasource PostgreSQL to your Grafana, configuring Host (usually localhost:5432), Database (mastats's DB you defined) and DB User fields. 

Then you could graph your Mastodon server stats with above Grafana's PostgreSQL datasource!
It gets all needed data from Mastodon's Postgresql database and then store stats to the mastats's Postgresql database created above, to feed Grafana with their values.

What values can you track in Grafana?

- Local users
- New local users / hour / day / week
- Local posts
- Local posts / hour
- Posts per user
- Current week activity (interactions)
- Active users
- Federated servers
- New federated servers / hour
- Disc space used by the Mastodon's database
- Database disc space increase / hour
